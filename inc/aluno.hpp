#ifndef ALUNO_HPP
#define ALUNO_HPP

#include <string>
#include "pessoa.hpp"

using namespace std;

class Aluno : public Pessoa {

private:
	string curso;
	float ira;

public:
	Aluno();
	~Aluno();

	float getIra();
	void calculaIra();
	string getCurso();
	void setCurso(string curso);


}

#endif
