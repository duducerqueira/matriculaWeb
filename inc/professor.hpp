#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP

#include <string>
#include "pessoa.hpp"

using namespace std;

class Professor : public Pessoa{

private:
	string formacao;
	string departamento;
	float indiceDeAprovacao;
public:
	Professor();
	~Professor();

	string getFormacao();
	void setFormacao(string formacao);
	string getDepartamento();
	void setDepartamento(string departamento);
	float getIndiceDeAprovacao();
	void setIndiceDeAprovacao(float indiceDeAprovacao);


}

#endif
